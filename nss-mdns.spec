Name:            nss-mdns
Version:         0.15.1
Release:         3
Summary:         Host name resolution for GNU Name Service Switch
License:         LGPLv2+
URL:             https://github.com/lathiat/nss-mdns
Source0:         https://github.com/lathiat/nss-mdns/releases/download/v%{version}/nss-mdns-%{version}.tar.gz

BuildRequires:   make gcc pkgconfig(check)
Requires:        avahi

%description
Nss-mdns is a plugin for the GNU Name Service Switch (NSS) functionality of the
GNU C Library (glibc) providing host name resolution via Multicast DNS (aka Zeroconf,
aka Apple Rendezvous, aka Apple Bonjour), effectively allowing name resolution by
common Unix/Linux programs in the ad-hoc mDNS domain .local.

%package_help

%prep
%autosetup -n %{name}-%{version}

%build
%configure --libdir=/%{_lib}
%make_build

%install
%make_install

%check
make check || (cat ./test-suite.log; false)

%pre

%preun
if [ "$1" -eq 0 -a -f /etc/nsswitch.conf ] ; then
	sed -i.bak '
		/^hosts:/ !b
		s/[[:blank:]]\+mdns\(4\|6\)\?\(_minimal\( \[NOTFOUND=return\]\)\?\)\?//g
	' /etc/nsswitch.conf
fi

%post
/sbin/ldconfig
if [ -f /etc/nsswitch.conf ] ; then
	sed -i.bak '
		/^hosts:/ !b
		/\<mdns\(4\|6\)\?\(_minimal\)\?\>/ b
		s/\([[:blank:]]\+\)dns\>/\1mdns4_minimal [NOTFOUND=return] dns/g
		' /etc/nsswitch.conf
fi

%postun
/sbin/ldconfig

%files
%defattr(-,root,root)
%license LICENSE
/%{_lib}/*.so.*

%files help
%doc README.md NEWS.md ACKNOWLEDGEMENTS.md

%changelog
* Wed Mar 02 2022 wulei <wulei80@huawei.com> - 0.15.1-3
- Add preun and post

* Fri Jan 07 2022 wulei <wulei80@huawei.com> - 0.15.1-2
- Add help package

* Tue Dec 28 2021 wulei <wulei80@huawei.com> - 0.15.1-1
- Package update

* Fri Nov 8 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.14.1-3
- Package init
